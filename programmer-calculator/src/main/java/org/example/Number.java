package org.example;

public interface Number {
    int getValueDec();
    String toBin();
    String toOct();
    String toDec();
    String toHex();
    Number copy();
}

package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {
    public static char[] getSupportMemoryOperation() {
        return new char[]{'+', '-', '*', '/'};
    }

    enum Notation {
        bin,
        oct,
        dec,
        hex
    }

    private Notation notation;
    private Number result;
    public Calculator(String expression, Notation notation, Number memoryResult, String memoryResultOperation) throws Exception {
        this.notation = notation;
        result = calculateExpression(expression);
        result = calculateAction(memoryResult, result, memoryResultOperation);
    }

    public Calculator(String expression, Notation notation) throws Exception {
        this.notation = notation;
        result = calculateExpression(expression);
    }

    public Number calculateExpression(String expression) throws Exception {
        // Преобразование выражения в обратную польскую запись
        Stack<String> stack = new Stack<>();
        List<String> output = new ArrayList<>();
        try {
            List<String> tokens_temp = Arrays.stream(expression.split("(?<=[-+*/%()])|(?=[-+*/%()])")).toList();
            List<String> tokens = new ArrayList<>();

            for (String token_temp : tokens_temp) {
                for (String t : (token_temp.split("(?<=<<)|(?=<<)|(?<=>>)|(?=>>)")))
                    tokens.add(t);
            }

            tokens_temp = (List<String>) ((ArrayList<String>) tokens).clone();
            tokens.clear();
            for (String token_temp : tokens_temp) {
                for (String t : (token_temp.split("(?<=!&)|(?=!&)")))
                    tokens.add(t);
            }

            tokens_temp = (List<String>) ((ArrayList<String>) tokens).clone();
            tokens.clear();
            for (String token_temp : tokens_temp) {
                for (String t : (token_temp.split("(?<=!\\|)|(?=!\\|)")))
                    tokens.add(t);
            }

            tokens_temp = (List<String>) ((ArrayList<String>) tokens).clone();
            for (String token_temp : tokens_temp) {
                final Pattern pattern = Pattern.compile("!\\d+", Pattern.MULTILINE);
                final Matcher matcher = pattern.matcher(token_temp);
                if (matcher.matches()) tokens.clear();
                if (matcher.matches())
                    for (String t : (token_temp.split("(?=!)|(?<=!)")))
                        tokens.add(t);
            }

            tokens_temp = (List<String>) ((ArrayList<String>) tokens).clone();
            for (String token_temp : tokens_temp) {
                final Pattern pattern = Pattern.compile("\\d+\\|\\d+", Pattern.MULTILINE);
                final Matcher matcher = pattern.matcher(token_temp);
                if (matcher.matches()) tokens.clear();
                if (matcher.matches())
                    for (String t : (token_temp.split("(?<=\\|)|(?=\\|)")))
                        tokens.add(t);
            }

            tokens_temp = (List<String>) ((ArrayList<String>) tokens).clone();
            for (String token_temp : tokens_temp) {
                final Pattern pattern = Pattern.compile("\\d+&\\d+", Pattern.MULTILINE);
                final Matcher matcher = pattern.matcher(token_temp);
                if (matcher.matches()) tokens.clear();
                if (matcher.matches())
                    for (String t : (token_temp.split("(?<=&)|(?=&)")))
                        tokens.add(t);
            }

            for (String token : tokens) {
                if (token.matches("^[0-9A-Fa-f]+$")) {
                    output.add(token);
                } else if (token.equals("(")) {
                    stack.push(token);
                } else if (token.equals(")")) {
                    while (!stack.peek().equals("(")) {
                        output.add(stack.pop());
                    }
                    stack.pop();
                } else {
                    while (!stack.isEmpty() && getPrecedence(token) <= getPrecedence(stack.peek())) {
                        output.add(stack.pop());
                    }
                    stack.push(token);
                }
            }
            while (!stack.isEmpty()) {
                output.add(stack.pop());
            }
        }
        catch (Exception e) {
            throw new Exception("Не удалось обработать выражение");
        }

        // Вычисление значения выражения
        Stack<Number> resultStack = new Stack<>();
        Number memlock = null;
        for (String token : output) {
            if (token.matches("^[0-9A-Fa-f]+$")) {
                try {
                    switch (notation) {
                        case bin:
                            resultStack.push(new NumberBin(token));
                            break;
                        case oct:
                            resultStack.push(new NumberOct(token));
                            break;
                        case dec:
                            resultStack.push(new NumberDec(token));
                            break;
                        case hex:
                            resultStack.push(new NumberHex(token));
                            break;
                    }
                }
                catch (Exception e) {
                    throw new Exception("Не удалось обработать " + token + ", в выбранной системе счисления");
                }
            } else {
                boolean isUnary = false;
                Number operand1 = new NumberDec("0");
                Number operand2 = null;
                try {
                    operand2 = resultStack.pop();
                }
                catch (Exception e) {
                    throw new Exception("Не удалось получить результат");
                }

                try {
                    operand1 = resultStack.pop();
                }
                catch (Exception e) {
                    isUnary = true;
                    if (memlock != null){
                        operand1 = memlock.copy();
                        isUnary = false;
                    }
                    memlock = null;
                }
                if (isUnary) {
                    switch (token) {
                        case "!":
                            resultStack.push(inv(operand2));
                            break;
                        case "-":
                            memlock = new NumberDec(String.valueOf(-operand2.getValueDec()));
                            break;
                    }
                }
                else {
                    resultStack.push(calculateAction(operand1, operand2, token));
                }
            }
        }
        return resultStack.pop();
    }

    private Number calculateAction (Number operand1, Number operand2, String token) throws Exception {
        switch (token) {
            case "+":
                return add(operand1, operand2);
            case "-":
                return sub(operand1, operand2);
            case "*":
                return mul(operand1, operand2);
            case "/":
                try {
                    return div(operand1, operand2);
                }
                catch (Exception e) {
                    throw new Exception("Деление на ноль");
                }
            case "%":
                return remainderDiv(operand1, operand2);
            case "<<":
                return shiftLeft(operand1, operand2);
            case ">>":
                return shiftRight(operand1, operand2);
            case "|":
                return or(operand1, operand2);
            case "&":
                return and(operand1, operand2);
            case "^":
                return xor(operand1, operand2);
            case "!|":
                return notOr(operand1, operand2);
            case "!&":
                return notAnd(operand1, operand2);
        }
        return null;
    }

    private static int getPrecedence(String operator) {
        switch (operator) {
            case "+":
            case "-":
                return 1;
            case "*":
            case "/":
            case "%":
            case "<<":
            case ">>":
            case "|":
            case "&":
            case "!|":
            case "!&":
            case "^":
                return 2;
            case "!":
                return 3;
            default:
                return 0;
        }
    }

    private static Number add (Number operand1, Number operand2) {
        return new NumberDec(String.valueOf(
                operand1.getValueDec() + operand2.getValueDec()
        ));
    }

    private static Number sub (Number operand1, Number operand2) {
        return new NumberDec(String.valueOf(
                operand1.getValueDec() - operand2.getValueDec()
        ));
    }

    private static Number mul (Number operand1, Number operand2) {
        return new NumberDec(String.valueOf(
                operand1.getValueDec() * operand2.getValueDec()
        ));
    }

    private static Number div (Number operand1, Number operand2) {
        return new NumberDec(String.valueOf(
                operand1.getValueDec() / operand2.getValueDec()
        ));
    }

    private static Number remainderDiv (Number operand1, Number operand2) {
        return new NumberDec(String.valueOf(
                operand1.getValueDec() % operand2.getValueDec()
        ));
    }

    private static Number shiftLeft(Number num, Number shift) {
        return new NumberDec(String.valueOf(
                num.getValueDec() << shift.getValueDec()
        ));
    }

    private static Number shiftRight(Number num, Number shift) {
        return new NumberDec(String.valueOf(
                num.getValueDec() >> shift.getValueDec()
        ));
    }

    private static Number inv(Number num) {
        return new NumberDec(String.valueOf(
                -num.getValueDec()-1
        ));
    }

    private static Number and (Number operand1, Number operand2) {
        return new NumberDec(String.valueOf(
                operand1.getValueDec() & operand2.getValueDec()
        ));
    }

    private static Number or (Number operand1, Number operand2) {
        return new NumberDec(String.valueOf(
                operand1.getValueDec() | operand2.getValueDec()
        ));
    }

    private static Number notAnd (Number operand1, Number operand2) {
        return inv(
                new NumberDec(String.valueOf(
                    operand1.getValueDec() & operand2.getValueDec()
        )));
    }

    private static Number notOr (Number operand1, Number operand2) {
        return inv(
                new NumberDec(String.valueOf(
                    operand1.getValueDec() | operand2.getValueDec()
        )));
    }

    private static Number xor (Number operand1, Number operand2) {
        return new NumberDec(String.valueOf(
                operand1.getValueDec() ^ operand2.getValueDec()
        ));
    }

    public Number getResult() {
        return result;
    }
}

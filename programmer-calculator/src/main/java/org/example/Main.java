package org.example;

import java.util.Scanner;

public class Main {
    static MemoryCalculationSave memoryCalculationSave = new MemoryCalculationSave();
    public static void main(String[] args) {
        System.out.println("Данный калькулятор поддерживает операции:");
        System.out.println("сложения (+), вычитания (-), умножения (*), деления (/), остаток от деления (%)");
        System.out.println("Логические побитовые операции:");
        System.out.println( "НЕ (!)\n" +
                            "И (&)\n" +
                            "ИЛИ (|)\n" +
                            "И-НЕ (!&)\n" +
                            "ИЛИ-НЕ (!|)\n" +
                            "Исключающее ИЛИ (^)");
        System.out.println("Операторы побитового сдвига:");
        System.out.println("Сдвиг вправо (>>)");
        System.out.println("Сдвиг влево (<<)");

        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Введите выражение: ");
            String expression = scanner.next();
            expression = removeSpace(expression);
            System.out.println("Выберите систему счисления, которая была использована в выражении");
            System.out.println("1. Двоичная");
            System.out.println("2. Восьмеричная");
            System.out.println("3. Десятеричная");
            System.out.println("4. Шестнадцатиричная");
            scanner = new Scanner(System.in);
            int notationValue = scanner.nextInt();
            Calculator.Notation notation = Calculator.Notation.dec;
            if (notationValue == 1) notation = Calculator.Notation.bin;
            else if (notationValue == 2) notation = Calculator.Notation.oct;
            else if (notationValue == 3) notation = Calculator.Notation.dec;
            else if (notationValue == 4) notation = Calculator.Notation.hex;
            else {
                System.out.println("Введено некорректное значение. Выбрана система счисления по умолчанию - десятеричная");
            }

            Number result = null;
            Calculator calculator = null;

            if (isLoadMemory(expression)) {
                Number memoryResult = memoryCalculationSave.getLast();
                String memoryResultOperation = String.valueOf(expression.charAt(1));
                expression = expression.substring(2,expression.length());

                try {
                    calculator = new Calculator(expression, notation, memoryResult, memoryResultOperation);
                    result = calculator.getResult();
                }
                catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            else {
                try {
                    calculator = new Calculator(expression, notation);
                    result = calculator.getResult();
                }
                catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }

            if (result != null && calculator != null) {
                System.out.println("Результат вычислений");
                System.out.println("Двоичная система счисления:\t\t\t\t" + result.toBin());
                System.out.println("Восьмеричная система счисления:\t\t\t" + result.toOct());
                System.out.println("Десятеричная система счисления:\t\t\t" + result.toDec());
                System.out.println("Шестнадцатиричная система счисления:\t" + result.toHex());
            }
            try {
                memoryCalculationSave.addCalculation(calculator.getResult());
            }
            catch (Exception e) {
                System.out.println("Результат вычислений не был добавлен в память");
            }

            System.out.println("Завершить выполнение (да / нет)");
            scanner = new Scanner(System.in);
            String exitValue = scanner.next();
            if (exitValue.toLowerCase().charAt(0) == 'д' && exitValue.toLowerCase().charAt(1) == 'а' && exitValue.length() == 2) break;
        }
    }

    private static String removeSpace (String expression) {
        return expression.replaceAll(" ", "");
    }

    private static boolean isLoadMemory (String expression) {
        if (expression.toLowerCase().charAt(0) == 'm') {
            char[] supportOperation = Calculator.getSupportMemoryOperation();
            for (char item: supportOperation) {
                if (expression.charAt(1) == item) return true;
            }
        }
        return false;
    }
}
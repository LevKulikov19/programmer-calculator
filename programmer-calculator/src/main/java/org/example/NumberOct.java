package org.example;

public class NumberOct implements Number {
    private int value;
    NumberOct(String number) {
        value = Integer.parseInt(number, 8);
    }

    @Override
    public int getValueDec() {
        return value;
    }

    @Override
    public String toBin() {
        return Integer.toBinaryString(value);
    }

    @Override
    public String toOct() {
        return Integer.toOctalString(value);
    }

    @Override
    public String toDec() {
        return String.valueOf(value);
    }

    @Override
    public String toHex() {
        return Integer.toHexString(value);
    }
    @Override
    public Number copy() {
        return new NumberOct(String.valueOf(value));
    }
}

package org.example;

public class NumberBin implements Number{
    private int value;
    NumberBin(String number) {
        value = Integer.parseInt(number, 2);
    }

    @Override
    public int getValueDec() {
        return value;
    }

    @Override
    public String toBin() {
        return Integer.toBinaryString(value);
    }

    @Override
    public String toOct() {
        return Integer.toOctalString(value);
    }

    @Override
    public String toDec() {
        return String.valueOf(value);
    }

    @Override
    public String toHex() {
        return Integer.toHexString(value);
    }

    @Override
    public Number copy() {
        return new NumberBin(String.valueOf(value));
    }
}
